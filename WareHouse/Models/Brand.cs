using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace WareHouse.Models.ViewModels
{
    public class Brand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreateTime { get; set;} = DateTime.Now;
        
        public string  UserId{ get; set; }
        public  User  User { get; set; }


        
    }
}

