using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace WareHouse.Models.ViewModels
{
    public class MinusQuantityProduct
    {
        public int Id { get; set; }
        public int Quantity{ get; set; }
        
        public bool IsDeleted { get; set; } = false;
        
        public int ProductId { get; set; }
        public Product Product { get; set; }
        
        public int MinusMovementId { get; set; }
        public MinusMovement MinusMovement { get; set; }
        

    }
}

