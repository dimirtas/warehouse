using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using WareHouse.Models.ViewModels;

namespace WareHouse.Models
{
    public class User  : IdentityUser
    {
       
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }
       
        
        public List<Brand> Brands { get; set; } =  new();
        public List<Provider> Providers { get; set; } =  new();
        public List<Recipient> Recipients { get; set; } =  new();
        public List<ProductType> ProductTypes { get; set; } =  new();
        public List<Parameter> Parameters { get; set; } = new();
        public List<Movement> Movements { get; set; } =  new();
        
    }
}