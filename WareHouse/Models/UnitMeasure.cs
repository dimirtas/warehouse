using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace WareHouse.Models.ViewModels
{
    public class UnitMeasure
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; } = false;

    }
}

