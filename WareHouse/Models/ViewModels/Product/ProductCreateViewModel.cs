﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class ProductCreateViewModel
    {
        [Required(ErrorMessage = "Название Продукта не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckProductName", controller: "Validation", ErrorMessage = "Данное Название Продукта уже зарегестрировано")]
        public string Name { get; set; }
        public IFormFile Foto { get; set; }
        public string Description { get; set; }
        
        [Required(ErrorMessage = "поле Артикул не заполнено")]
        [Display(Name = "UniqueCode")]
        [Remote(action: "CheckProductUniqueCode", controller: "Validation", ErrorMessage = "Данный артикул уже зарегестрирован")]
        public string UniqueCode { get; set; }
        public int? BrandId { get; set; }
        public int? UnitMeasureId { get; set; }
        public int? ProductTypeId{ get; set; }
       
    
    }
    
}
