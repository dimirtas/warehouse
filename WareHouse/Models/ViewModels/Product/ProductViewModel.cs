﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class ProductViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Название Параметра не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckProductNameEdit",controller: "Validation", AdditionalFields = "Id", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }

        [Display(Name = "Foto")]
        [Remote(action: "CheckImage", controller: "Validation", ErrorMessage = "Не верный формат файла")]
        public IFormFile Foto { get; set; }
        public string Description { get; set; }
        
        [Required(ErrorMessage = "поле Артикул не заполнено")]
        [Display(Name = "UniqueCode")]
        [Remote(action: "CheckProductUniqueCodeEdit", controller: "Validation", AdditionalFields = "Id", ErrorMessage = "Данный артикул уже зарегестрирован")]
        public string UniqueCode { get; set; }
        public int? ProductTypeId{ get; set; }
        public int? BrandId { get; set; }
       
        /*[Required(ErrorMessage = "Выберите Ед.измерения ")]
        [Display(Name = "UnitMeasureId")]
        [Remote(action: "CheckProductUnitMeasureId", controller: "Validation", ErrorMessage = "Данное значение уже зарегестрировано")]*/
        public int? UnitMeasureId { get; set; }

        public List<InformationProductViewModel> InformationProductList { get; set; }


        
    }
}