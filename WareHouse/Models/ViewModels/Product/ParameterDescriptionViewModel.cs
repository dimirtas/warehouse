﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WareHouse.Models.ViewModels

{
    public class ParameterDescriptionViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DescriptionParam { get; set; }
        public int ProductId { get; set; }
    }
}