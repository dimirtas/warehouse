﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class ParameterCreateViewModel
    {
        [Required(ErrorMessage = "Название Параметра не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckParameterName", controller: "Validation", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }
        public int? ProductTypeId{ get; set; }
        
    
    }
    
}

