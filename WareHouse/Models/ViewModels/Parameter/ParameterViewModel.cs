﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class ParameterViewModel
    {
        
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Название Параметра не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckParameterNameEdit",controller: "Validation", AdditionalFields = "Id", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }
        
        public int? ProductTypeId { get; set; }
        
        
    }
}