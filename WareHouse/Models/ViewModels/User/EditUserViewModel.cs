﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class EditUserViewModel
    {
        
        // [Display(Name = "электронный адрес")]
        // [Required(ErrorMessage = "Не указан Email")]
        // [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Не верный формат email")]
        // public string Email { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Не указано имя")]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        
        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Не указано имя")]
        [DataType(DataType.Text)]
        public string Surname { get; set; }
        
        
    }
}