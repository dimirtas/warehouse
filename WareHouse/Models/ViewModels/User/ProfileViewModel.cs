﻿using System;

namespace WareHouse.Models.ViewModels
{
    public class ProfileViewModel
    {
        public string Id{ get; set; }
        public string UserName { get; set; }
        //public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        
    }
}