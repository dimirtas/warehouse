﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "значение не установлено")]
        [Display(Name = "Login")]
        [Remote(action: "CheckLogin", controller: "Validation", ErrorMessage = "Email или Login не верны")]
        public string Login { get; set; }
        
        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Your password")]
        public string Password { get; set; }
        
        [Display(Name = "Remember")]
        public bool RememerMe { get; set; }
        
        public string ReturnUrl { get; set; }
    }
}