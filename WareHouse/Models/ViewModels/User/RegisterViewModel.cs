﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class RegisterViewModel
    {
        // [Required(ErrorMessage = "значение не установлено")]
        // [Display(Name = "Email")]
        // [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Не верный формат email")]
        // [Remote(action: "CheckEmail", controller: "Validation", ErrorMessage = "Данный email уже зарегестрирован")]
        // public string Email { get; set; }
        
        
        [Required(ErrorMessage = "логин не заполнен")]
        [Display(Name = "UserName")]
        [Remote(action: "CheckUserName", controller: "Validation", ErrorMessage = "Данный Логин уже зарегестрирован")]
        public string UserName { get; set; }
        
        [Required(ErrorMessage = "введите имя")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "введит фамилию")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "что за на")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        
     
        [Required(ErrorMessage = "Пароль не совпадает")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }


        
    }
}