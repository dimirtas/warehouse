﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class AllProductsViewModel
        {
            public int Quantity{ get; set; }
        
            public int ProductId { get; set; }
            public Product Product { get; set; }

        }

}