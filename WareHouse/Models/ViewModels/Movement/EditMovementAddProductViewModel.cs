﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class EditMovementAddProductViewModel
    {
        public int MovementId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        
    }
}