﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class QuantityProductViewModel
        {
            public  int Id { get; set; }
            public  string ParameterName { get; set; }
            public  int ParameterId { get; set; }
            public  string DescriptionInformation { get; set; }
        }

}