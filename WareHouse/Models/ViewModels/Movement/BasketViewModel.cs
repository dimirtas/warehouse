﻿using System.Collections.Generic;

namespace WareHouse.Models.ViewModels
{
    public class BasketViewModel
    {
        public int Summ { get; set; }
        public  List<BasketProductViewModel> BasketProductViewModels { get; set; }
    }

    public class BasketProductViewModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string NameProduct{ get; set; }
        public int Quantity { get; set; }
        public int MovementId  { get; set; }
       
    }
}