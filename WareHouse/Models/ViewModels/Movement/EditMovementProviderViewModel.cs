﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class EditMovementProviderViewModel
    {
        public int MovementId { get; set; }
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
    }
}