﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WareHouse.Models.ViewModels

{
    public class MovementEditingReceiptViewModel
    {
        public int Id { get; set; }
        public string Document { get; set; }
        public string ProviderName { get; set;} 
        public List<Product> Products { get; set; }
        public BasketViewModel BasketExist { get; set; }
       
        public List<QuantityProductAddViewModel> QuantityProducts { get; set; }
        
    }
}