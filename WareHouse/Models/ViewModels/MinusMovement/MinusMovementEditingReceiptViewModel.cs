﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WareHouse.Models.ViewModels

{
    public class MinusMovementEditingReceiptViewModel
    {
        public int Id { get; set; }
        public string Document { get; set; }
        public string RecipientName { get; set;} 
        public List<Product> Products { get; set; }
        public MinusBasketViewModel BasketExist { get; set; }
       
        public List<MinusQuantityProductAddViewModel> MinusQuantityProducts { get; set; }
        
    }
}