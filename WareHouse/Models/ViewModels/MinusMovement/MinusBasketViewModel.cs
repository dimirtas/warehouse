﻿using System.Collections.Generic;

namespace WareHouse.Models.ViewModels
{
    public class MinusBasketViewModel
    {
        public int Summ { get; set; }
        public  List<MinusBasketProductViewModel> MinusBasketProductViewModels { get; set; }
    }

    public class MinusBasketProductViewModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string NameProduct{ get; set; }
        public int Quantity { get; set; }
        public int MinusMovementId  { get; set; }
       
    }
}