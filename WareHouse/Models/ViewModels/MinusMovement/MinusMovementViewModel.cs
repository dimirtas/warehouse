﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class MinusMovementViewModel
    {
        public int Id { get; set; }
        public DateTime CreateTime { get; set;} = DateTime.Now;
        public DateTime UpdateTime { get; set;} 
        public string Document { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string  UserName { get; set; }
        public string RecipientName { get; set; }
    }
}