﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WareHouse.Models.ViewModels

{
    public class MinusMovementDetailViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите название , бренда")]
        public string Name { get; set; }
        public string Foto { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateTime { get; set;} 
        public string Description { get; set; }
        public string UniqueCode { get; set; }
        public User User { get; set; }
        public Brand Brand { get; set; }
        public ProductType ProductType { get; set; }
        public UnitMeasure UnitMeasure { get; set; }
        
        public List<MinusQuantityProduct> MinusQuantityProducts { get; set; }
        
    }
}