﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class EditMinusMovementRecipientViewModel
    {
        public int MinusMovementId { get; set; }
        public int RecipientId { get; set; }
        public string RecipientName { get; set; }
    }
}