﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class EditMinusMovementAddProductViewModel
    {
        public int MinusMovementId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        
    }
}