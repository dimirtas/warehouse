﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WareHouse.Models.ViewModels

{
    public class MinusQuantityProductAddViewModel
    {
        public int Id { get; set; }
        public int Quantity{ get; set; }
        public int ProductId { get; set; }
        public int MinusMovementId { get; set; }
       
    }
}