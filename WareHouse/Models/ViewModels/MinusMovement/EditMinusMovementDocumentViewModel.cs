﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class EditMinusMovementDocumentViewModel
    {
        public int MinusMovementId { get; set; }
        public string Document { get; set; }

    }
}