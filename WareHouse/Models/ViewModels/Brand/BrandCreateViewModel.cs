﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class BrandCreateViewModel
    {
        [Required(ErrorMessage = "Название бренда не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckBrand", controller: "Validation", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }
    
    }
    
}

