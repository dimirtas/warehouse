﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class RecipientViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Название  не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckRecipientName", controller: "Validation", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "БИН  не заполнен")]
        [Display(Name = "BIN")]
        [Remote(action: "CheckRecipientBin", controller: "Validation", ErrorMessage = "Введите 12 цифр или данный БИН уже зарегестрирован")]
        public string Bin { get; set; }
    }
}