﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WareHouse.Models.ViewModels

{
    public class RecipientDetailViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите название , бренда")]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateTime { get; set;} 
        public string Bin { get; set; }
        public User User { get; set; }
    }
}