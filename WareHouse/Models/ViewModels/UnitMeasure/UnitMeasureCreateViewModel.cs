﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class UnitMeasureCreateViewModel
    {
        [Required(ErrorMessage = "Название не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckUnitMeasure", controller: "Validation", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }
    
    }
    
}

