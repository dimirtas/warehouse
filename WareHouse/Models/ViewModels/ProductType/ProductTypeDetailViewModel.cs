﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WareHouse.Models.ViewModels

{
    public class ProductTypeDetailViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите название , бренда")]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateTime { get; set;} 
        public User User { get; set; }

        private List<ParametersProduct> Parameters { get; set; } 
    }

    public class ParametersProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    
    
}