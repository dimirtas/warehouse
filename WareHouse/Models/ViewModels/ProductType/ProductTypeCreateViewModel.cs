﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels
{
    public class ProductTypeCreateViewModel
    {
        [Required(ErrorMessage = "Название Типа Товара не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckProductType", controller: "Validation", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }


    }
    
}

