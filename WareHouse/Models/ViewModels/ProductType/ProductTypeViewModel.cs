﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WareHouse.Models.ViewModels

{
    public class ProductTypeViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Название Типа Товара не заполнено")]
        [Display(Name = "Name")]
        [Remote(action: "CheckProductType", controller: "Validation", ErrorMessage = "Данное значение уже зарегестрировано")]
        public string Name { get; set; }
    }
}