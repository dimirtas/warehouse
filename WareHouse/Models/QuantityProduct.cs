using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace WareHouse.Models.ViewModels
{
    public class QuantityProduct
    {
        public int Id { get; set; }
        public int Quantity{ get; set; }
        
        public int ProductId { get; set; }
        public Product Product { get; set; }
        
        public int MovementId { get; set; }
        public Movement Movement { get; set; }
        
      

    }
}

