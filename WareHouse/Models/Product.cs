using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace WareHouse.Models.ViewModels
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Foto { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreateTime { get; set;} = DateTime.Now;
        public string Description { get; set; }
        public string UniqueCode { get; set; }
        
        public string  UserId { get; set; }
        public  User  User { get; set; }

        public int? BrandId { get; set; }
        public Brand Brand { get; set; }
        
        public int? ProductTypeId { get; set; }
        public ProductType ProductType { get; set; }
        
        public int? UnitMeasureId { get; set; }
        public UnitMeasure UnitMeasure { get; set; }
        
        
        public List<InformationProduct> InformationProducts { get; set; }
        public List<Movement> Movements { get; set; }

    }
}

