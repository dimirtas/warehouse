using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace WareHouse.Models.ViewModels
{
    public class InformationProduct
    {
        public int Id { get; set; }
        
        public int? ProductId { get; set; }
        public Product Product { get; set; }
        
        public int? ParameterId { get; set; }
        public Parameter Parameter { get; set; }
        
        public string Description { get; set; }

    }
}

