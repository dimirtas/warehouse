using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace WareHouse.Models.ViewModels
{
    public class Movement
    {
        public int Id { get; set; }
        public DateTime CreateTime { get; set;} = DateTime.Now;
        public DateTime UpdateTime { get; set;} 
        public string Document { get; set; }
          //   public bool isArrived { get; set; }
        public bool IsDeleted { get; set; } = false;
          
        public string  UserId { get; set; }
        public  User  User { get; set; }

        public int? ProviderId { get; set; }
        public Provider Provider { get; set; }
        
          // public int? RecipientId { get; set; }
          // public Recipient Recipient { get; set; }
        
        public List<QuantityProduct> QuantityProducts { get; set; }

    }
}

