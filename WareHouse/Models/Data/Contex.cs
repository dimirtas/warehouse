using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models.ViewModels;

namespace WareHouse.Models.Data
{
    public class Context:IdentityDbContext<User>
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<UnitMeasure> UnitMeasures { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Recipient> Recipients { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<InformationProduct> InformationProducts { get; set; }
        public DbSet<QuantityProduct> QuantityProducts { get; set; }
        public DbSet<Movement> Movements { get; set; }
        public DbSet<MinusMovement>  MinusMovements { get; set; }
        public DbSet<MinusQuantityProduct> MinusQuantityProducts { get; set; }
        
       


        public Context(DbContextOptions<Context> options) : base(options)
        {
        }


    }
}

