using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class ParametersController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ParametersController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Parameter> parameters = await _db.Parameters.Where(x=>x.IsDeleted == false).ToListAsync();

            List<ParameterViewModel> models = new List<ParameterViewModel>();

            if (parameters.Count > 0)
            {
                foreach (var parameter in parameters)
                {
                    models.Add(new ParameterViewModel()
                    {
                        Id = parameter.Id,
                        Name = parameter.Name,
                        
                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            SelectList typeList = new SelectList(_db.ProductTypes.Where(x=>x.IsDeleted == false ), "Id", "Name");
            if(typeList.ToList().Count == 0)
                return RedirectToAction("Index", "ProductTypes");
            ViewBag.TypeList = typeList;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(ParameterCreateViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                
                Parameter parameter = new Parameter()
                {
                    Name = model.Name,
                    UserId = user.Id,
                    ProductTypeId = model.ProductTypeId,
                    
                    
                };

                _db.Entry(parameter).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var parameter = _db.Parameters.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            var user =  _db.Users.FirstOrDefault(p => p.Id == parameter.UserId);
            var productType =  _db.ProductTypes.FirstOrDefault(p => p.Id == parameter.ProductTypeId);

            if (parameter  is null)
                return NotFound();

            ParameterDetailViewModel model = new ParameterDetailViewModel();
            model.Id = parameter.Id;
            model.Name = parameter.Name;
            model.CreateTime = parameter.CreateTime;
            model.User = user;
            model.ProductType = productType;
            

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var parameter = _db.Parameters.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            

            if (parameter  is null)
                return NotFound();

            ParameterViewModel model = new ParameterViewModel();
            model.Id = parameter.Id;
            model.Name = parameter.Name;
            model.ProductTypeId = parameter.ProductTypeId;
            
            SelectList typeList = new SelectList(_db.ProductTypes.Where(x=>x.IsDeleted == false ), "Id", "Name", "parameter.ProductTypeId");
            ViewBag.TypeList = typeList;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ParameterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var parameterChange = _db.Parameters.FirstOrDefault(p => p.Id == model.Id && p.IsDeleted == false);
                if (parameterChange  is null)
                    return NotFound();
                parameterChange.Name = model.Name;
                parameterChange.UserId = user.Id;
                parameterChange.ProductTypeId = model.ProductTypeId;
                    
                _db.Entry(parameterChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("Index", "Parameters");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Parameter parameter = await _db.Parameters.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (parameter != null)
                {
                    return View(parameter);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                Parameter parameter = await _db.Parameters.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                parameter.IsDeleted = true;                                    // мягкое удаление
                _db.Entry(parameter).State = EntityState.Modified; 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }


    }

}