using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class MinusMovementsController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public MinusMovementsController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<MinusMovement> minusMovements = await _db.MinusMovements.Where(x=>x.IsDeleted == false).ToListAsync();
            List<Recipient> recipients = await _db.Recipients.Where(x=>x.IsDeleted == false).ToListAsync();
            List<User> users = await _db.Users.ToListAsync();

            List<MinusMovementViewModel> models = new List<MinusMovementViewModel>();

            if (minusMovements.Count > 0)
            {
                foreach (var minusMovement in minusMovements)
                {
                    var recipient = recipients.FirstOrDefault(x => x.Id == minusMovement.RecipientId );
                    var user = users.FirstOrDefault(x => x.Id == minusMovement.UserId );
                    models.Add(new MinusMovementViewModel()
                    {
                        Id = minusMovement.Id,
                        Document = minusMovement.Document,
                        RecipientName = recipient.Name,
                        UserName = user.Name,
                        
                    });
                }
            }
            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            SelectList recipientList = new SelectList(_db.Recipients.Where(x=>x.IsDeleted == false ), "Id", "Name");
            if(recipientList.ToList().Count == 0)
                return RedirectToAction("Index", "MinusMovements");
            
            ViewBag.RecipientList = recipientList;
            
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(MinusMovementCreateViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                MinusMovement minusMovement = new MinusMovement()
                {
                    Document = model.Document,
                    RecipientId = model.RecipientId,
                    CreateTime =  DateTime.Now,
                    UserId = user.Id,
                };
                minusMovement.UpdateTime = minusMovement.CreateTime;
                
                _db.Entry(minusMovement).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Edit", "MinusMovements", new {minusMovement.Id});
            }
            return RedirectToAction("Index");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> СreatingReceipt(int id)
        {
            var minusMovement = _db.MinusMovements.FirstOrDefault(m => m.Id == id && m.IsDeleted == false);
            var recipient = _db.Recipients.FirstOrDefault(p => p.Id == minusMovement.RecipientId);
            
            if (minusMovement  is null)
                return NotFound();

            MinusMovementCreatingReceiptViewModel model = new MinusMovementCreatingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = minusMovement.Id;
            model.Document = minusMovement.Document;
            model.RecipientName = recipient.Name;
            model.Products = products;
            
            //существующий список товар в накладной
            model.BasketExist = GetBasket(minusMovement.Id);

            return View(model);
        }
        
        public MinusBasketViewModel GetBasket(int minusMovementId)
        {
            MinusBasketViewModel basketViewModel = new MinusBasketViewModel();
            List<MinusBasketProductViewModel> minusBasketProductViewModels = new List<MinusBasketProductViewModel>();

            // список выбраных товаров конктреной корзины, этого пользователя и этой накладной
            List<MinusQuantityProduct> minusQuantityProducts = _db.MinusQuantityProducts
                .Include(q => q.Product)
                .Where( b => b.MinusMovementId == minusMovementId ).ToList();
            List<Product> products = _db.Products.ToList();

            int basketSumm = 0;

            // если список пуст, то в созданную basketViewModel не чего не добавится и модель пустая уйдет на предствление
            // если не пустая, то наполнится товарами ранее выбранными

            foreach (var minusQuantityProduct in  minusQuantityProducts)
            {
               
               minusBasketProductViewModels.Add(new MinusBasketProductViewModel()
                    {
                        Id = minusQuantityProduct.Id,
                        Quantity = minusQuantityProduct.Quantity,
                        ProductId = minusQuantityProduct.ProductId,
                        NameProduct = minusQuantityProduct.Product.Name,
                        MinusMovementId = minusMovementId,
                    }
                );
                basketSumm = basketSumm +  minusQuantityProduct.Quantity; // итого
            }

            basketViewModel.MinusBasketProductViewModels = minusBasketProductViewModels;
            basketViewModel.Summ = basketSumm;
            return basketViewModel;
        }
        
         [HttpPost]
        public async Task<IActionResult> AddToBasket(int productId, int minusMovementId) //Ajax
        {
            
            //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            MinusBasketViewModel basketViewModel = GetBasket(minusMovementId);

            // выбранный товар
            var currentProduct = _db.Products.FirstOrDefault(c => c.Id == productId);

            // проверка входит ли выбранный товар  в корзину
            var existProduct = basketViewModel.MinusBasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);

            // если текущая модель уже содержить выбранный товар
            if (existProduct != null)
            {
                //изменение модели 
                existProduct.Quantity = existProduct.Quantity + 1;
                basketViewModel.Summ = basketViewModel.Summ + 1;

                //сохранить изменения корзины в db
                var saveMinusQuantityProduct = _db.MinusQuantityProducts.FirstOrDefault(q=>q.Id == existProduct.Id);
                saveMinusQuantityProduct.Quantity =  existProduct.Quantity;

                _db.Entry(saveMinusQuantityProduct).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
            else
            {
                MinusQuantityProduct minusQuantityProduct = new MinusQuantityProduct();
                minusQuantityProduct.ProductId = currentProduct.Id;
                minusQuantityProduct.Quantity = 1;
                minusQuantityProduct.MinusMovementId = minusMovementId;
                
                _db.Entry(minusQuantityProduct).State = EntityState.Added;
                await _db.SaveChangesAsync();

                basketViewModel.MinusBasketProductViewModels.Add(new MinusBasketProductViewModel()
                    {
                        Id = minusQuantityProduct.Id,             // здесь присваиваем свежеполученный ID в таблице minusQuantityProduct
                        ProductId = currentProduct.Id,
                        Quantity = 1,
                        NameProduct = currentProduct.Name,
                    }
                );
                basketViewModel.Summ = basketViewModel.Summ + 1;
            }
            
            return PartialView("PartialView/_basketPartialMinus", basketViewModel);
        }
        

        public async Task<IActionResult> DelFromBasket(int minusMovementId, int productId) //Ajax
        {
           //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            MinusBasketViewModel basketViewModel = GetBasket(minusMovementId);

            // проверка входит ли выбранное товар в корзину
            var existProduct = basketViewModel.MinusBasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);
            // если текущая модель уже содержить выбранный продукт
            if (existProduct != null)
            {
                if (existProduct.Quantity > 1)
                {
                    //уменьшение кол-во на 1
                    existProduct.Quantity = existProduct.Quantity - 1;
                    basketViewModel.Summ = basketViewModel.Summ - 1;

                    //сохранить изменения корзины в db
                    
                    var saveMinusQuantityProduct = _db.MinusQuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);
                    saveMinusQuantityProduct.Quantity = existProduct.Quantity;;

                    _db.Entry(saveMinusQuantityProduct).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
                else
                {
                    //находим нужную позицию в корзине и удаляем  в db
                    var saveMinusQuantityProduct = _db.MinusQuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);

                    _db.Entry(saveMinusQuantityProduct).State = EntityState.Deleted;
                    await _db.SaveChangesAsync();
                    basketViewModel = GetBasket(minusMovementId);
                }
            }
            return PartialView("PartialView/_basketPartialMinus", basketViewModel);
        }
        
        // вариант удаления минус одна еденица за клик из позиции в корзине
        public async Task<IActionResult> DelFromBasketEdit(int minusMovementId, int productId) //Ajax
        {
            var minusMovement = _db.MinusMovements.FirstOrDefault(m => m.Id == minusMovementId && m.IsDeleted == false);
            var recipient = _db.Recipients.FirstOrDefault(p => p.Id == minusMovement.RecipientId);
            
            if (minusMovement  is null)
                return NotFound();

            MinusMovementEditingReceiptViewModel model = new MinusMovementEditingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = minusMovement.Id;
            model.Document = minusMovement.Document;
            model.RecipientName = recipient.Name;
            model.Products = products;
            
            //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            model.BasketExist = GetBasket(minusMovementId);

            // проверка входит ли выбранное товар в корзину
            var existProduct = model.BasketExist.MinusBasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);
            // если текущая модель уже содержить выбранный продукт
            if (existProduct != null)
            {
                if (existProduct.Quantity > 1)
                {
                    //уменьшение кол-во на 1
                    existProduct.Quantity = existProduct.Quantity - 1;
                    model.BasketExist.Summ = model.BasketExist.Summ - 1;

                    //сохранить изменения корзины в db
                    
                    var saveMinusQuantityProduct = _db.MinusQuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);
                    saveMinusQuantityProduct.Quantity = existProduct.Quantity;;

                    _db.Entry(saveMinusQuantityProduct).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
                else
                {
                    //находим нужную позицию в корзине и удаляем  в db
                    var saveMinusQuantityProduct = _db.MinusQuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);

                    _db.Entry(saveMinusQuantityProduct).State = EntityState.Deleted;
                    await _db.SaveChangesAsync();
                    model.BasketExist = GetBasket(minusMovementId);
                }
            }

            return PartialView("PartialView/_basketPartialEditMinus", model);
        }
        
        // вариант удаления целой позиции из корзины 
         public async Task<IActionResult> DelPositionFromBasketEdit(int minusMovementId, int productId) //Ajax
        {
            var minusMovement = _db.MinusMovements.FirstOrDefault(m => m.Id == minusMovementId && m.IsDeleted == false);
            var recipient = _db.Recipients.FirstOrDefault(p => p.Id == minusMovement.RecipientId);
            
            if (minusMovement  is null)
                return NotFound();

            MinusMovementEditingReceiptViewModel model = new MinusMovementEditingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = minusMovement.Id;
            model.Document = minusMovement.Document;
            model.RecipientName = recipient.Name;
            model.Products = products;
            
            //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            model.BasketExist = GetBasket(minusMovementId);

            // проверка входит ли выбранное товар в корзину
            var existProduct = model.BasketExist.MinusBasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);
            // если текущая модель уже содержить выбранный продукт
            if (existProduct != null)
            {
                  //находим нужную позицию в корзине и удаляем  в db
                    var saveMinusQuantityProduct = _db.MinusQuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);

                    _db.Entry(saveMinusQuantityProduct).State = EntityState.Deleted;
                    await _db.SaveChangesAsync();
                    model.BasketExist = GetBasket(minusMovementId);
            }

            return PartialView("PartialView/_basketPartialEditMinus", model);
        }

        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            
            var minusMovement = _db.MinusMovements.FirstOrDefault(m => m.Id == id && m.IsDeleted == false);
            var recipient = _db.Recipients.FirstOrDefault(p => p.Id == minusMovement.RecipientId);
            
            if (minusMovement  is null)
                return NotFound();

            MinusMovementEditingReceiptViewModel model = new MinusMovementEditingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = minusMovement.Id;
            model.Document = minusMovement.Document;
            model.RecipientName = recipient.Name;
            model.Products = products;
            
            //существующий список товар в накладной
            model.BasketExist = GetBasket(minusMovement.Id); 
            
            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> Edit (MinusMovementEditingReceiptViewModel model)
        {
            if (model.BasketExist != null)
            {
               // return NotFound();

                // список выбраных товаров конкретной корзины, этой накладной
                List<MinusQuantityProduct> minusQuantityProducts = _db.MinusQuantityProducts
                    .Include(q => q.Product)
                    .Where(b => b.MinusMovementId == model.Id).ToList();
                List<Product> products = _db.Products.ToList();

                List<MinusQuantityProduct> minusQuantityProductsChange = new List<MinusQuantityProduct>();

                for (int i = 0; i < model.BasketExist.MinusBasketProductViewModels.Count; i++)
                {
                    var minusQuantityProduct = minusQuantityProducts.FirstOrDefault(
                        x =>
                            x.ProductId == model.BasketExist.MinusBasketProductViewModels[i].ProductId &&
                            x.MinusMovementId == model.BasketExist.MinusBasketProductViewModels[i].MinusMovementId);

                    minusQuantityProduct.Quantity = model.BasketExist.MinusBasketProductViewModels[i].Quantity;
                    minusQuantityProductsChange.Add(minusQuantityProduct);
                }

                // проверка если нет изменений
                if (minusQuantityProductsChange.Count == 0)
                    return RedirectToAction("Index");

                _db.MinusQuantityProducts.UpdateRange(minusQuantityProductsChange);
                await _db.SaveChangesAsync();
            }


            return RedirectToAction("Index");
        }

        
        [HttpGet]
        public async Task<IActionResult> RecipientEdit (int Id)
        {
            var minusMovement = _db.MinusMovements.FirstOrDefault(p => p.Id == Id && p.IsDeleted == false);
            var recipient = _db.Recipients.FirstOrDefault(p => p.Id == minusMovement.RecipientId);
            
            if (minusMovement  is null)
                return NotFound();

            EditMinusMovementRecipientViewModel model = new EditMinusMovementRecipientViewModel();
            model.MinusMovementId = minusMovement.Id;
            model.RecipientId = (int)minusMovement.RecipientId;
            model.RecipientName = recipient.Name;
            
            SelectList recipientList = new SelectList
                (_db.Recipients.Where(x=>x.IsDeleted == false ), "Id", "Name", "recipient.Id");
            ViewBag.RecipientList = recipientList;

            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> RecipientEdit (EditMinusMovementRecipientViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var minusMovementChange = _db.MinusMovements.FirstOrDefault(p => p.Id == model.MinusMovementId && p.IsDeleted == false);
               
                if (minusMovementChange  is null)
                    return NotFound();

                minusMovementChange.RecipientId = model.RecipientId;

                _db.Entry(minusMovementChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                
                return RedirectToAction("Edit", "MinusMovements", new {minusMovementChange.Id});
            }
            
          return RedirectToAction("Index", "MinusMovements");
        }
        
        [HttpGet]
        public async Task<IActionResult> DocumentEdit (int Id)
        {
            var minusMovement = _db.MinusMovements.FirstOrDefault(p => p.Id == Id && p.IsDeleted == false);
            
            if (minusMovement  is null)
                return NotFound();

            EditMinusMovementDocumentViewModel model = new EditMinusMovementDocumentViewModel();
            model.MinusMovementId = minusMovement.Id;
            model.Document = minusMovement.Document;
            
            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> DocumentEdit (EditMinusMovementDocumentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var minusMovementChange = _db.MinusMovements.FirstOrDefault(p => p.Id == model.MinusMovementId && p.IsDeleted == false);
               
                if (minusMovementChange  is null)
                    return NotFound();

                minusMovementChange.Document = model.Document;

                _db.Entry(minusMovementChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                
                return RedirectToAction("Edit", "MinusMovements", new {minusMovementChange.Id});
            }
            
            return RedirectToAction("Index", "MinusMovements");
        }

        [HttpGet]
        public async Task<IActionResult> AddProductToReceipt (int Id)
        {
            var minusMovement = _db.MinusMovements.FirstOrDefault(p => p.Id == Id && p.IsDeleted == false);
           
            if (minusMovement  is null)
                return NotFound();

            EditMinusMovementAddProductViewModel model = new EditMinusMovementAddProductViewModel();
            model.MinusMovementId = minusMovement.Id;
            
            
            SelectList productList = new SelectList
                (_db.Products.Where(x=>x.IsDeleted == false ), "Id", "Name");
            ViewBag.RecipientList = productList;

            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> AddProductToReceipt (EditMinusMovementAddProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
                MinusBasketViewModel basketViewModel = GetBasket(model.MinusMovementId);
                var minusMovementChange = _db.MinusMovements.FirstOrDefault(p => p.Id == model.MinusMovementId && p.IsDeleted == false);

                // выбранный товар
              /////////////  var currentProduct = _db.Products.FirstOrDefault(c => c.Id == model.ProductId);

                // проверка входит ли выбранный товар  в корзину
                var existProduct = basketViewModel.MinusBasketProductViewModels.FirstOrDefault(b => b.ProductId == model.ProductId);

                // если текущая модель уже содержить выбранный товар
                if (existProduct != null)
                {
                    //изменение модели 
                    existProduct.Quantity = existProduct.Quantity + model.Quantity;

                    //сохранить изменения корзины в db
                    var saveMinusQuantityProduct = _db.MinusQuantityProducts.FirstOrDefault(q=>q.Id == existProduct.Id);
                    saveMinusQuantityProduct.Quantity =  existProduct.Quantity;

                    _db.Entry(saveMinusQuantityProduct).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
                else
                {
                    MinusQuantityProduct minusQuantityProduct = new MinusQuantityProduct();
                    minusQuantityProduct.ProductId = model.ProductId;
                    minusQuantityProduct.Quantity = model.Quantity;
                    minusQuantityProduct.MinusMovementId = model.MinusMovementId;
                    
                    _db.Entry(minusQuantityProduct).State = EntityState.Added;
                    await _db.SaveChangesAsync();
                }
                return RedirectToAction("Edit", "MinusMovements", new {minusMovementChange.Id});
            }
            
            return RedirectToAction("Index", "MinusMovements");
        }
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                MinusMovement minusMovement = await _db.MinusMovements.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (minusMovement != null)
                {
                    return View(minusMovement);
                }
            }
        
            return NotFound();
        }
        
        
        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                MinusMovement minusMovement = await _db.MinusMovements.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                List<MinusQuantityProduct> minusQuantityProducts = _db.MinusQuantityProducts
                    .Where(b => b.MinusMovementId == minusMovement.Id).ToList();
               
                if(minusQuantityProducts != null)
                    foreach (var minusQuantityProduct in minusQuantityProducts)
                    {
                        _db.Entry(minusQuantityProduct).State = EntityState.Deleted;
                    }
                    
                _db.Entry(minusMovement).State = EntityState.Deleted; 
                
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
        
            return NotFound();
        }
       
    }

}