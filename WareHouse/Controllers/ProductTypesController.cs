using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class ProductTypesController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ProductTypesController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<ProductType> productTypes = await _db.ProductTypes.Where(x=>x.IsDeleted == false).ToListAsync();

            List<ProductTypeViewModel> models = new List<ProductTypeViewModel>();

            if (productTypes.Count > 0)
            {
                foreach (var productType in productTypes)
                {
                    models.Add(new ProductTypeViewModel()
                    {
                        Id = productType.Id,
                        Name = productType.Name,
                        
                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            SelectList paramList = new SelectList(_db.Parameters.Where(x=>x.IsDeleted == false ), "Id", "Name");
            ViewBag.ParamList = paramList;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(ProductTypeCreateViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                
                ProductType productType = new ProductType()
                {
                    Name = model.Name,
                    UserId = user.Id,
                    
                    
                };

                _db.Entry(productType).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var productType = _db.ProductTypes.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            var user =  _db.Users.FirstOrDefault(p => p.Id == productType.UserId);

            if (productType  is null)
                return NotFound();

            ProductTypeDetailViewModel model = new ProductTypeDetailViewModel();
            model.Id = productType.Id;
            model.Name = productType.Name;
            model.CreateTime = productType.CreateTime;
            model.User = user;
            

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var productType = _db.ProductTypes.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            

            if (productType  is null)
                return NotFound();

            ProductTypeViewModel model = new ProductTypeViewModel();
            model.Id = productType.Id;
            model.Name = productType.Name;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ProductTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var productTypeChange = _db.ProductTypes.FirstOrDefault(p => p.Id == model.Id && p.IsDeleted == false);
                if (productTypeChange  is null)
                    return NotFound();
                productTypeChange.Name = model.Name;
                productTypeChange.UserId = user.Id;
                
                _db.Entry(productTypeChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("Index", "ProductTypes");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                ProductType productType = await _db.ProductTypes.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (productType != null)
                {
                    return View(productType);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                ProductType productType = await _db.ProductTypes.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                productType.IsDeleted = true;                                    // мягкое удаление
                _db.Entry(productType).State = EntityState.Modified; 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }


    }

}