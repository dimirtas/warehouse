using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class ProfileController : Controller
    {
        private Context _db;

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;


        public ProfileController(Context db, UserManager<User> userManager, SignInManager<User> signInManager,
            IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> PersonalProfile()
        {
            ProfileViewModel model = new ProfileViewModel();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                model.Id = user.Id;
                model.UserName = user.UserName;
               // model.Email = user.Email;
                model.Name = user.Name;
                model.Surname = user.Surname;
                
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> EditProfile()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            EditUserViewModel model = new EditUserViewModel();
            if (user != null)
            {
               // model.Email = user.Email;
                model.Name = user.Name;
                model.Surname = user.Surname;
                
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditProfile(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userChange = await _userManager.FindByNameAsync(User.Identity.Name);

               // userChange.Email = model.Email;
                userChange.Name = model.Name;
                userChange.Surname = model.Surname;


                _db.Entry(userChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("PersonalProfile", "Profile");
        }
        
    }
}