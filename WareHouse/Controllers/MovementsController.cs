using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class MovementsController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public MovementsController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Movement> movements = await _db.Movements.Where(x=>x.IsDeleted == false).ToListAsync();
            List<Provider> providers = await _db.Providers.Where(x=>x.IsDeleted == false).ToListAsync();
            List<User> users = await _db.Users.ToListAsync();

            List<MovementViewModel> models = new List<MovementViewModel>();

            if (movements.Count > 0)
            {
                foreach (var movement in movements)
                {
                    var provider = providers.FirstOrDefault(x => x.Id == movement.ProviderId );
                    var user = users.FirstOrDefault(x => x.Id == movement.UserId );
                    models.Add(new MovementViewModel()
                    {
                        Id = movement.Id,
                        Document = movement.Document,
                        ProviderName = provider.Name,
                        UserName = user.Name,
                        
                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            SelectList providerList = new SelectList(_db.Providers.Where(x=>x.IsDeleted == false ), "Id", "Name");
            if(providerList.ToList().Count == 0)
                return RedirectToAction("Index", "Movements");
            
            ViewBag.ProviderList = providerList;
            
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(MovementCreateViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                Movement movement = new Movement()
                {
                    Document = model.Document,
                    ProviderId = model.ProviderId,
                    CreateTime =  DateTime.Now,
                    UserId = user.Id,
                };
                movement.UpdateTime = movement.CreateTime;
                
                _db.Entry(movement).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Edit", "Movements", new {movement.Id});
                // return RedirectToAction("СreatingReceipt", "Movements", new {movement.Id});
            }
            return RedirectToAction("Index");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> СreatingReceipt(int id)
        {
            var movement = _db.Movements.FirstOrDefault(m => m.Id == id && m.IsDeleted == false);
            var provider = _db.Providers.FirstOrDefault(p => p.Id == movement.ProviderId);
            
            if (movement  is null)
                return NotFound();

            MovementCreatingReceiptViewModel model = new MovementCreatingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = movement.Id;
            model.Document = movement.Document;
            model.ProviderName = provider.Name;
            model.Products = products;
            
            //существующий список товар в накладной
            model.BasketExist = GetBasket(movement.Id);

            return View(model);
        }
        
        public BasketViewModel GetBasket(int movementId)
        {
            BasketViewModel basketViewModel = new BasketViewModel();
            List<BasketProductViewModel> basketProductViewModels = new List<BasketProductViewModel>();

            // список выбраных товаров конктреной корзины, этого пользователя и этой накладной
            List<QuantityProduct> quantityProducts = _db.QuantityProducts
                .Include(q => q.Product)
                .Where( b => b.MovementId == movementId ).ToList();
            List<Product> products = _db.Products.ToList();

            int basketSumm = 0;

            // если список пуст, то в созданную basketViewModel не чего не добавится и модель пустая уйдет на предствление
            // если не пустая, то наполнится товарами ранее выбранными

            foreach (var quantityProduct in  quantityProducts)
            {
               
                basketProductViewModels.Add(new BasketProductViewModel()
                    {
                        Id = quantityProduct.Id,
                        Quantity = quantityProduct.Quantity,
                        ProductId = quantityProduct.ProductId,
                        NameProduct = quantityProduct.Product.Name,
                        MovementId = movementId,
                    }
                );
                basketSumm = basketSumm +  quantityProduct.Quantity; // итого
            }

            basketViewModel.BasketProductViewModels = basketProductViewModels;
            basketViewModel.Summ = basketSumm;
            return basketViewModel;
        }
        
         [HttpPost]
        public async Task<IActionResult> AddToBasket(int productId, int movementId) //Ajax
        {
            
            //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            BasketViewModel basketViewModel = GetBasket(movementId);

            // выбранный товар
            var currentProduct = _db.Products.FirstOrDefault(c => c.Id == productId);

            // проверка входит ли выбранный товар  в корзину
            var existProduct = basketViewModel.BasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);

            // если текущая модель уже содержить выбранный товар
            if (existProduct != null)
            {
                //изменение модели 
                existProduct.Quantity = existProduct.Quantity + 1;
                basketViewModel.Summ = basketViewModel.Summ + 1;

                //сохранить изменения корзины в db
                var saveQuantityProduct = _db.QuantityProducts.FirstOrDefault(q=>q.Id == existProduct.Id);
                saveQuantityProduct.Quantity =  existProduct.Quantity;

                _db.Entry(saveQuantityProduct).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
            else
            {
                QuantityProduct quantityProduct = new QuantityProduct();
                quantityProduct.ProductId = currentProduct.Id;
                quantityProduct.Quantity = 1;
                quantityProduct.MovementId = movementId;
                
                _db.Entry(quantityProduct).State = EntityState.Added;
                await _db.SaveChangesAsync();

                basketViewModel.BasketProductViewModels.Add(new BasketProductViewModel()
                    {
                        Id = quantityProduct.Id,             // здесь присваиваем свежеполученный ID в таблице quantityProduct
                        ProductId = currentProduct.Id,
                        Quantity = 1,
                        NameProduct = currentProduct.Name,
                    }
                );
                basketViewModel.Summ = basketViewModel.Summ + 1;
            }
            
            return PartialView("PartialView/_basketPartial", basketViewModel);
        }
        

        public async Task<IActionResult> DelFromBasket(int movementId, int productId) //Ajax
        {
           //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            BasketViewModel basketViewModel = GetBasket(movementId);

            // проверка входит ли выбранное товар в корзину
            var existProduct = basketViewModel.BasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);
            // если текущая модель уже содержить выбранный продукт
            if (existProduct != null)
            {
                if (existProduct.Quantity > 1)
                {
                    //уменьшение кол-во на 1
                    existProduct.Quantity = existProduct.Quantity - 1;
                    basketViewModel.Summ = basketViewModel.Summ - 1;

                    //сохранить изменения корзины в db
                    
                    var saveQuantityProduct = _db.QuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);
                    saveQuantityProduct.Quantity = existProduct.Quantity;;

                    _db.Entry(saveQuantityProduct).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
                else
                {
                    //находим нужную позицию в корзине и удаляем  в db
                    var saveQuantityProduct = _db.QuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);

                    _db.Entry(saveQuantityProduct).State = EntityState.Deleted;
                    await _db.SaveChangesAsync();
                    basketViewModel = GetBasket(movementId);
                }
            }
            return PartialView("PartialView/_basketPartial", basketViewModel);
        }
        
        // вариант удаления минус одна еденица за клик из позиции в корзине
        public async Task<IActionResult> DelFromBasketEdit(int movementId, int productId) //Ajax
        {
            var movement = _db.Movements.FirstOrDefault(m => m.Id == movementId && m.IsDeleted == false);
            var provider = _db.Providers.FirstOrDefault(p => p.Id == movement.ProviderId);
            
            if (movement  is null)
                return NotFound();

            MovementEditingReceiptViewModel model = new MovementEditingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = movement.Id;
            model.Document = movement.Document;
            model.ProviderName = provider.Name;
            model.Products = products;
            
            //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            model.BasketExist = GetBasket(movementId);

            // проверка входит ли выбранное товар в корзину
            var existProduct = model.BasketExist.BasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);
            // если текущая модель уже содержить выбранный продукт
            if (existProduct != null)
            {
                if (existProduct.Quantity > 1)
                {
                    //уменьшение кол-во на 1
                    existProduct.Quantity = existProduct.Quantity - 1;
                    model.BasketExist.Summ = model.BasketExist.Summ - 1;

                    //сохранить изменения корзины в db
                    
                    var saveQuantityProduct = _db.QuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);
                    saveQuantityProduct.Quantity = existProduct.Quantity;;

                    _db.Entry(saveQuantityProduct).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
                else
                {
                    //находим нужную позицию в корзине и удаляем  в db
                    var saveQuantityProduct = _db.QuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);

                    _db.Entry(saveQuantityProduct).State = EntityState.Deleted;
                    await _db.SaveChangesAsync();
                    model.BasketExist = GetBasket(movementId);
                }
            }

            return PartialView("PartialView/_basketPartialEdit", model);
        }
        
        // вариант удаления целой позиции из корзины 
        public async Task<IActionResult> DelPositionFromBasketEdit(int movementId, int productId) //Ajax
        {
            var movement = _db.Movements.FirstOrDefault(m => m.Id == movementId && m.IsDeleted == false);
            var provider = _db.Providers.FirstOrDefault(p => p.Id == movement.ProviderId);
            
            if (movement  is null)
                return NotFound();

            MovementEditingReceiptViewModel model = new MovementEditingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = movement.Id;
            model.Document = movement.Document;
            model.ProviderName = provider.Name;
            model.Products = products;
            
            //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
            model.BasketExist = GetBasket(movementId);

            // проверка входит ли выбранное товар в корзину
            var existProduct = model.BasketExist.BasketProductViewModels.FirstOrDefault(b => b.ProductId == productId);
            // если текущая модель уже содержить выбранный продукт
            if (existProduct != null)
            {
                //находим нужную позицию в корзине и удаляем  в db
                    var saveQuantityProduct = _db.QuantityProducts.FirstOrDefault(q => q.Id == existProduct.Id);

                    _db.Entry(saveQuantityProduct).State = EntityState.Deleted;
                    await _db.SaveChangesAsync();
                    model.BasketExist = GetBasket(movementId);
             
            }

            return PartialView("PartialView/_basketPartialEdit", model);
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var movement = _db.Movements.FirstOrDefault(m => m.Id == id && m.IsDeleted == false);
            var provider = _db.Providers.FirstOrDefault(p => p.Id == movement.ProviderId);
            
            if (movement  is null)
                return NotFound();

            MovementEditingReceiptViewModel model = new MovementEditingReceiptViewModel();
            var products = _db.Products.ToList();
            model.Id = movement.Id;
            model.Document = movement.Document;
            model.ProviderName = provider.Name;
            model.Products = products;
            
            //существующий список товар в накладной
            model.BasketExist = GetBasket(movement.Id); 
            
            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> Edit (MovementEditingReceiptViewModel model)
        {
            if (model.BasketExist != null)
            {
               
                // список выбраных товаров конкретной корзины, этой накладной
                List<QuantityProduct> quantityProducts = _db.QuantityProducts
                    .Include(q => q.Product)
                    .Where(b => b.MovementId == model.Id).ToList();
                List<Product> products = _db.Products.ToList();

                List<QuantityProduct> quantityProductsChange = new List<QuantityProduct>();

                for (int i = 0; i < model.BasketExist.BasketProductViewModels.Count; i++)
                {
                    var quantityProduct = quantityProducts.FirstOrDefault(
                        x =>
                            x.ProductId == model.BasketExist.BasketProductViewModels[i].ProductId &&
                            x.MovementId == model.BasketExist.BasketProductViewModels[i].MovementId);

                    quantityProduct.Quantity = model.BasketExist.BasketProductViewModels[i].Quantity;
                    quantityProductsChange.Add(quantityProduct);
                }

                // проверка если нет изменений
                if (quantityProductsChange.Count == 0)
                    return RedirectToAction("Index");

                _db.QuantityProducts.UpdateRange(quantityProductsChange);
                await _db.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        
        [HttpGet]
        public async Task<IActionResult> ProviderEdit (int Id)
        {
            var movement = _db.Movements.FirstOrDefault(p => p.Id == Id && p.IsDeleted == false);
            var provider = _db.Providers.FirstOrDefault(p => p.Id == movement.ProviderId);
            
            if (movement  is null)
                return NotFound();

            EditMovementProviderViewModel model = new EditMovementProviderViewModel();
            model.MovementId = movement.Id;
            model.ProviderId = (int)movement.ProviderId;
            model.ProviderName = provider.Name;
            
            SelectList providerList = new SelectList
                (_db.Providers.Where(x=>x.IsDeleted == false ), "Id", "Name", "provider.Id");
            ViewBag.ProviderList = providerList;

            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> ProviderEdit (EditMovementProviderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var movementChange = _db.Movements.FirstOrDefault(p => p.Id == model.MovementId && p.IsDeleted == false);
               
                if (movementChange  is null)
                    return NotFound();

                movementChange.ProviderId = model.ProviderId;

                _db.Entry(movementChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                
                return RedirectToAction("Edit", "Movements", new {movementChange.Id});
            }
            
          return RedirectToAction("Index", "Movements");
        }
        
        [HttpGet]
        public async Task<IActionResult> DocumentEdit (int Id)
        {
            var movement = _db.Movements.FirstOrDefault(p => p.Id == Id && p.IsDeleted == false);
            
            if (movement  is null)
                return NotFound();

            EditMovementDocumentViewModel model = new EditMovementDocumentViewModel();
            model.MovementId = movement.Id;
            model.Document = movement.Document;
            
            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> DocumentEdit (EditMovementDocumentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var movementChange = _db.Movements.FirstOrDefault(p => p.Id == model.MovementId && p.IsDeleted == false);
               
                if (movementChange  is null)
                    return NotFound();

                movementChange.Document = model.Document;

                _db.Entry(movementChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                
                return RedirectToAction("Edit", "Movements", new {movementChange.Id});
            }
            
            return RedirectToAction("Index", "Movements");
        }
        
       
        
        [HttpGet]
        public async Task<IActionResult> AddProductToReceipt (int Id)
        {
            var movement = _db.Movements.FirstOrDefault(p => p.Id == Id && p.IsDeleted == false);
           
            if (movement  is null)
                return NotFound();

            EditMovementAddProductViewModel model = new EditMovementAddProductViewModel();
            model.MovementId = movement.Id;
            
            
            SelectList productList = new SelectList
                (_db.Products.Where(x=>x.IsDeleted == false ), "Id", "Name");
            ViewBag.ProviderList = productList;

            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> AddProductToReceipt (EditMovementAddProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                //  получение модели через метод  GetBasket, если есть уже добавленны блюда вернет их иначе создаст почти пустую модель
                BasketViewModel basketViewModel = GetBasket(model.MovementId);
                var movementChange = _db.Movements.FirstOrDefault(p => p.Id == model.MovementId && p.IsDeleted == false);

                // выбранный товар
              /////////////  var currentProduct = _db.Products.FirstOrDefault(c => c.Id == model.ProductId);

                // проверка входит ли выбранный товар  в корзину
                var existProduct = basketViewModel.BasketProductViewModels.FirstOrDefault(b => b.ProductId == model.ProductId);

                // если текущая модель уже содержить выбранный товар
                if (existProduct != null)
                {
                    //изменение модели 
                    existProduct.Quantity = existProduct.Quantity + model.Quantity;

                    //сохранить изменения корзины в db
                    var saveQuantityProduct = _db.QuantityProducts.FirstOrDefault(q=>q.Id == existProduct.Id);
                    saveQuantityProduct.Quantity =  existProduct.Quantity;

                    _db.Entry(saveQuantityProduct).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
                else
                {
                    QuantityProduct quantityProduct = new QuantityProduct();
                    quantityProduct.ProductId = model.ProductId;
                    quantityProduct.Quantity = model.Quantity;
                    quantityProduct.MovementId = model.MovementId;
                    
                    _db.Entry(quantityProduct).State = EntityState.Added;
                    await _db.SaveChangesAsync();
                }
                return RedirectToAction("Edit", "Movements", new {movementChange.Id});
            }
            
            return RedirectToAction("Index", "Movements");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Movement movement = await _db.Movements.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (movement != null)
                {
                    return View(movement);
                }
            }
        
            return NotFound();
        }
        
        
        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                Movement movement = await _db.Movements.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                List<QuantityProduct> quantityProducts = _db.QuantityProducts
                    .Where(b => b.MovementId == movement.Id).ToList();
                
                if(quantityProducts != null)
                    foreach (var quantityProduct in quantityProducts)
                    {
                        _db.Entry(quantityProduct).State = EntityState.Deleted;
                    }
                _db.Entry(movement).State = EntityState.Deleted; 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
        
            return NotFound();
        }
       
    }

}