using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class  ReportsController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ReportsController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            // Приходы

            List<QuantityProduct> quantityProducts = _db.QuantityProducts
                .Include(q => q.Product)
                .ToList();

            List<AllProductsViewModel> models = new List<AllProductsViewModel>();

            if ( quantityProducts.Count > 0)
            {
                foreach (var quantityProduct in quantityProducts)
                {
                    var  existProduct = models.FirstOrDefault(m => m.ProductId == quantityProduct.ProductId);
                    if (existProduct != null)
                    {
                        //изменение модели
                       var currentProduct = models.FirstOrDefault(x => x.ProductId == quantityProduct.ProductId);
                       currentProduct.Quantity   += quantityProduct.Quantity;
                    }
                    else
                    {
                        // добавляем позицию в модель
                        models.Add(new AllProductsViewModel()
                        {
                            ProductId = quantityProduct.ProductId,
                            Product = quantityProduct.Product,
                            Quantity = quantityProduct.Quantity
                        });
                    }
                }
            }
            
            // расходы

            List<MinusQuantityProduct> minusQuantityProducts = _db.MinusQuantityProducts
                .Include(q => q.Product)
                .ToList();

            List<AllProductsViewModel> minusModels = new List<AllProductsViewModel>();

            if ( minusQuantityProducts.Count > 0)
            {
                foreach (var minusQuantityProduct in minusQuantityProducts)
                {
                    var  existProduct = minusModels.FirstOrDefault(m => m.ProductId == minusQuantityProduct.ProductId);
                    if (existProduct != null)
                    {
                        //изменение модели
                        var currentProduct = minusModels.FirstOrDefault(x => x.ProductId == minusQuantityProduct.ProductId);
                        currentProduct.Quantity   += minusQuantityProduct.Quantity;
                    }
                    else
                    {
                        // добавляем позицию в модель
                        minusModels.Add(new AllProductsViewModel()
                        {
                            ProductId = minusQuantityProduct.ProductId,
                            Product = minusQuantityProduct.Product,
                            Quantity = minusQuantityProduct.Quantity
                        });
                    }
                }
            }
            
            // итоговый подсчет
            
            if (minusModels.Count > 0)
              if (models.Count > 0)
              {
                foreach (var minusModel in minusModels)
                {
                    var  existProduct = models.FirstOrDefault(m => m.ProductId == minusModel.ProductId);
                    if (existProduct != null)
                    {
                        //изменение модели
                        var currentProduct = models.FirstOrDefault(x => x.ProductId == minusModel.ProductId);
                        currentProduct.Quantity   -= minusModel.Quantity;
                    }
                    else
                    {
                        // добавляем позицию в модель
                        models.Add(new AllProductsViewModel()
                        {
                            ProductId = minusModel.ProductId,
                            Product = minusModel.Product,
                            Quantity = - minusModel.Quantity
                        });
                    }
                }
              }
              else
              {
                  foreach (var minusModel in minusModels)
                  {
                      minusModel.Quantity = - minusModel.Quantity ;
                      models.Add(minusModel);
                  }
              }
            

            return View(models);
        }
        
        
        
        
        // [HttpGet]
        // public async Task<IActionResult> Add()
        // {
        //     return View();
        // }
        //
        // [HttpPost]
        // public async Task<IActionResult> Add(RecipientCreateViewModel model)
        // {
        //     var user = await _userManager.FindByNameAsync(User.Identity.Name);
        //     if (ModelState.IsValid)
        //     {
        //         
        //         Recipient recipient = new Recipient()
        //         {
        //             Name = model.Name,
        //             Bin = model.Bin,
        //             UserId = user.Id
        //             
        //             
        //         };
        //
        //         _db.Entry(recipient).State = EntityState.Added;
        //         await _db.SaveChangesAsync();
        //         return RedirectToAction("Index");
        //     }
        //
        //     return RedirectToAction("Index");
        // }

    }

}