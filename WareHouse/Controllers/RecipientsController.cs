using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class RecipientsController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public RecipientsController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Recipient> recipients = await _db.Recipients.Where(x=>x.IsDeleted == false).ToListAsync();

            List<RecipientViewModel> models = new List<RecipientViewModel>();

            if (recipients.Count > 0)
            {
                foreach (var recipient in recipients)
                {
                    models.Add(new RecipientViewModel()
                    {
                        Id = recipient.Id,
                        Name = recipient.Name,
                        Bin = recipient.Bin
                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(RecipientCreateViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                
                Recipient recipient = new Recipient()
                {
                    Name = model.Name,
                    Bin = model.Bin,
                    UserId = user.Id
                    
                    
                };

                _db.Entry(recipient).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var recipient = _db.Recipients.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            var user =  _db.Users.FirstOrDefault(p => p.Id == recipient.UserId);

            if (recipient  is null)
                return NotFound();

            RecipientDetailViewModel model = new RecipientDetailViewModel();
            model.Id = recipient.Id;
            model.Bin = recipient.Bin;
            model.Name = recipient.Name;
            model.CreateTime = recipient.CreateTime;
            model.User = user;
            

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var recipient = _db.Recipients.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            

            if (recipient  is null)
                return NotFound();

            RecipientViewModel model = new RecipientViewModel();
            model.Id = recipient.Id;
            model.Name = recipient.Name;
            model.Bin = recipient.Bin;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(RecipientViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var recipientChange = _db.Recipients.FirstOrDefault(p => p.Id == model.Id && p.IsDeleted == false);
                if (recipientChange  is null)
                    return NotFound();
                recipientChange.Name = model.Name;
                recipientChange.Bin = model.Bin;
                recipientChange.UserId = user.Id;
                
                _db.Entry(recipientChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("Index", "Recipients");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Recipient recipient = await _db.Recipients.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (recipient != null)
                {
                    return View(recipient);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                Recipient recipient = await _db.Recipients.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                recipient.IsDeleted = true;                                    // мягкое удаление
                _db.Entry(recipient).State = EntityState.Modified; 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }


    }

}