using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class ProductsController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ProductsController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Product> products = await _db.Products.Where(x=>x.IsDeleted == false).ToListAsync();

            List<ProductViewModel> models = new List<ProductViewModel>();

            if (products.Count > 0)
            {
                foreach (var product in products)
                {
                    models.Add(new ProductViewModel()
                    {
                        Id = product.Id,
                        Name = product.Name,
                        UniqueCode = product.UniqueCode,
                        Description = product.Description,

                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            SelectList typeList = new SelectList(_db.ProductTypes.Where(x=>x.IsDeleted == false ), "Id", "Name");
            SelectList brandList = new SelectList(_db.Brands.Where(x=>x.IsDeleted == false ), "Id", "Name");
            SelectList unitList = new SelectList(_db.UnitMeasures.Where(x=>x.IsDeleted == false ), "Id", "Name");
            if(brandList.ToList().Count == 0)
                return RedirectToAction("Index", "Brands");
            if(typeList.ToList().Count == 0)
                return RedirectToAction("Index", "ProductTypes");
            if(unitList.ToList().Count == 0)
                return RedirectToAction("Index", "UnitMeasures");
            ViewBag.TypeList = typeList;
            ViewBag.BrandList = brandList;
            ViewBag.UnitList = unitList;
            return View();
        }

       

        [HttpPost]
        public async Task<IActionResult> Add(ProductCreateViewModel model, IFormFile Foto)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                Product product = new Product()
                {
                    Name = model.Name,
                    UniqueCode = model.UniqueCode,
                    Description = model.Description,
                    UserId = user.Id,
                    ProductTypeId = model.ProductTypeId,
                    BrandId = model.BrandId,
                    UnitMeasureId = model.UnitMeasureId,
                };
                if (model.Foto != null && Foto.Length != 0)
                {
                    string exstension = Path.GetExtension(Foto.FileName);
                    string newFileName = Guid.NewGuid() + exstension;

                    string path = "/Files/" + newFileName;
                    
                    // сохраняем файл в папку Files в каталоге wwwroot
                    using (var fileStream = new FileStream(_webHostEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        Foto.CopyTo(fileStream);
                        product.Foto = path;
                    }
                }

                if (product.Foto == null)
                {
                    string path = "/Files/nofoto.jpg";
                    product.Foto = path;
                }

                _db.Entry(product).State = EntityState.Added;
                await _db.SaveChangesAsync();
                
                return RedirectToAction("DetailsFirst", "Products", new {product.Id});
            }

            return RedirectToAction("Index");
        }


         [HttpPost]
         public async Task<IActionResult> AddParameterDescription    (ProductDetailFirstViewModel model)
         {
            if (model.ParametersList.Count == 0)
                return NotFound();

            List<InformationProduct> informationList = new List<InformationProduct>();
            foreach (var param in model.ParametersList)
            {
                InformationProduct informationProduct = new InformationProduct();
                informationProduct.ParameterId = param.Id;
                informationProduct.ProductId = param.ProductId;
                informationProduct.Description = param.DescriptionParam;
                informationList.Add(informationProduct);
            }

            if (informationList.Count == 0)
                return NotFound();
            
             _db.InformationProducts.AddRange(informationList);
             await _db.SaveChangesAsync();

             return RedirectToAction("Index");
                     //  return Content("Success");
         }
         
        [HttpGet]
        public async Task<IActionResult> DetailsFirst(int id)
        {
            var product = _db.Products.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            var user =  _db.Users.FirstOrDefault(z => z.Id == product.UserId);
            var productType =  _db.ProductTypes.FirstOrDefault(z => z.Id == product.ProductTypeId);
            var brand =  _db.Brands.FirstOrDefault(z => z.Id == product.BrandId);
            var unit =  _db.UnitMeasures.FirstOrDefault(z => z.Id == product.UnitMeasureId);

            List<Parameter> parameters = _db.Parameters
                .Where(p => p.ProductTypeId == product.ProductTypeId && p.IsDeleted == false).ToList();

           
            List<ParameterDescriptionViewModel> mоdelParametrs = new List<ParameterDescriptionViewModel>();
            if (parameters !=null)
                foreach (var parameter in parameters)
                {
                    ParameterDescriptionViewModel parameterDescription = new ParameterDescriptionViewModel();
                    parameterDescription.Id = parameter.Id;
                    parameterDescription.Name = parameter.Name;
                    parameterDescription.DescriptionParam = "";
                    parameterDescription.ProductId = product.Id;
                    
                    mоdelParametrs.Add(parameterDescription);
                }

            if (product  is null)
                return NotFound();
         
            ProductDetailFirstViewModel model = new ProductDetailFirstViewModel();
            model.Id = product.Id;
            model.UniqueCode = product.UniqueCode;
            model.Name = product.Name; 
            model.Description = product.Description; 
            model.CreateTime = product.CreateTime;
            model.Foto = product.Foto;
            model.User = user;
            model.ProductType = productType;
            model.Brand = brand;
            model.UnitMeasure = unit;
            model.ParametersList = mоdelParametrs;
         
           
            
            return View(model);
        }
       
        
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var product = _db.Products.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            var user =  _db.Users.FirstOrDefault(z => z.Id == product.UserId);
            var productType =  _db.ProductTypes.FirstOrDefault(z => z.Id == product.ProductTypeId);
            var brand =  _db.Brands.FirstOrDefault(z => z.Id == product.BrandId);
            var unit =  _db.UnitMeasures.FirstOrDefault(z => z.Id == product.UnitMeasureId);

            List<Parameter> parameters = _db.Parameters
                .Where(p => p.ProductTypeId == product.ProductTypeId && p.IsDeleted == false).ToList();
            List<InformationProduct> informationProducts = _db.InformationProducts
                .Where(i => i.ProductId == id).ToList();

           
            List<InformationProductViewModel> mоdelInformationProducts = new List<InformationProductViewModel>();
            if (informationProducts !=null)
                foreach (var informationProductOne in informationProducts)
                {
                    InformationProductViewModel informationProduct = new InformationProductViewModel();
                    informationProduct.ParameterId = (int)informationProductOne.ParameterId;
                    informationProduct.ParameterName = informationProductOne.Parameter.Name;
                    informationProduct.DescriptionInformation = informationProductOne.Description ;
                    informationProduct.Id = informationProductOne.Id;
                    
                    mоdelInformationProducts.Add(informationProduct);
                }

            if (product  is null)
                return NotFound();
         
            ProductDetailViewModel model = new ProductDetailViewModel();
            model.Id = product.Id;
            model.UniqueCode = product.UniqueCode;
            model.Name = product.Name; 
            model.Description = product.Description; 
            model.CreateTime = product.CreateTime;
            model.Foto = product.Foto;
            model.User = user;
            model.ProductType = productType;
            model.Brand = brand;
            model.UnitMeasure = unit;
            model.InformationProducts =  mоdelInformationProducts;
         
           
            
            return View(model);
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var product = _db.Products.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            if (product  is null)
                return NotFound();
            
            List<Parameter> parameters = _db.Parameters
                .Where(p => p.ProductTypeId == product.ProductTypeId && p.IsDeleted == false).ToList();
            List<InformationProduct> informationlList = _db.InformationProducts.Where(i=> i.ProductId == id).ToList();

            ProductViewModel model = new ProductViewModel();
            model.Id = product.Id;
            model.Name = product.Name;
            model.Description = product.Description;
            model.UniqueCode = product.UniqueCode;
            model.ProductTypeId = product.ProductTypeId;
            model.UnitMeasureId = product.UnitMeasureId;
            model.BrandId = product.BrandId;
            model.InformationProductList = new List<InformationProductViewModel>();

            List<InformationProductViewModel> informationProductViewModels = new List<InformationProductViewModel>();
            if (informationlList.Count > 0)
            {
                foreach (var information in informationlList)
                {
                    
                    InformationProductViewModel informationProductViewModel = new InformationProductViewModel();
                    informationProductViewModel.Id = information.Id;
                    informationProductViewModel.DescriptionInformation = information.Description;
                    
                    var parameter = parameters.FirstOrDefault(x => x.Id == information.ParameterId);
                    informationProductViewModel.ParameterName = parameter.Name;
                    informationProductViewModel.ParameterId = parameter.Id;

                    informationProductViewModels.Add(informationProductViewModel);
                }

                model.InformationProductList = informationProductViewModels;
            }

            SelectList typeList = new SelectList(_db.ProductTypes.Where(x=>x.IsDeleted == false ), "Id", "Name", "product.ProductTypeId");
            SelectList brandList = new SelectList(_db.Brands.Where(x=>x.IsDeleted == false ), "Id", "Name","product.BrandId");
            SelectList unitList = new SelectList(_db.UnitMeasures.Where(x=>x.IsDeleted == false ), "Id", "Name","product.UnitMeasureId");
            ViewBag.TypeList = typeList;
            ViewBag.BrandList = brandList;
            ViewBag.UnitList = unitList;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ProductViewModel model,  IFormFile Foto)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var productChange = _db.Products.FirstOrDefault(p => p.Id == model.Id && p.IsDeleted == false);
                List<InformationProduct> listInformationChange = _db.InformationProducts
                    .Where(i => i.ProductId == model.Id).ToList();
                
                if (productChange  is null)
                    return NotFound();

                productChange.Name = model.Name;
                productChange.UserId = user.Id;
                productChange.Description = model.Description;
                productChange.UniqueCode = model.UniqueCode;
                productChange.ProductTypeId = model.ProductTypeId;
                productChange.UnitMeasureId = model.UnitMeasureId;
                productChange.BrandId = model.BrandId;
                
                if (model.Foto != null && Foto.Length != 0)
                {
                    string exstension = Path.GetExtension(Foto.FileName);
                    string newFileName = Guid.NewGuid() + exstension;

                    string path = "/Files/" + newFileName;
                    
                    // сохраняем файл в папку Files в каталоге wwwroot
                    using (var fileStream = new FileStream(_webHostEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        Foto.CopyTo(fileStream);
                        productChange.Foto = path;
                    }
                }
                
                if (model.InformationProductList.Count > 0)
                {
                   
                    for (int i = 0; i < model.InformationProductList.Count; i++)
                    {
                        if (listInformationChange[i].Id == model.InformationProductList[i].Id
                            && listInformationChange[i].ParameterId == model.InformationProductList[i].ParameterId)
                        {
                            listInformationChange[i].Description =
                                model.InformationProductList[i].DescriptionInformation;
                            _db.Entry(listInformationChange[i]).State = EntityState.Modified;
                        }
                    }
                }
               
                _db.Entry(productChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            return RedirectToAction("Index", "Products");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Product product = await _db.Products.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (product != null)
                {
                    return View(product);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                Product product = await _db.Products.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                product.IsDeleted = true;                                    // мягкое удаление
                _db.Entry(product).State = EntityState.Modified; 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }

    }

}