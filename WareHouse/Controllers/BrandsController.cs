using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class BrandsController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public BrandsController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Brand> brands = await _db.Brands.Where(x=>x.IsDeleted == false).ToListAsync();

            List<BrandViewModel> models = new List<BrandViewModel>();

            if (brands.Count > 0)
            {
                foreach (var brand in brands)
                {
                    models.Add(new BrandViewModel()
                    {
                        Id = brand.Id,
                        Name = brand.Name
                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> AddBrand()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddBrand(BrandCreateViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                
                Brand brand = new Brand()
                {
                    Name = model.Name,
                    UserId = user.Id
                };

                _db.Entry(brand).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var brand = _db.Brands.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            var user =  _db.Users.FirstOrDefault(p => p.Id == brand.UserId);

            if (brand  is null)
                return NotFound();

            BrandDetailViewModel model = new BrandDetailViewModel();
            model.Id = brand.Id;
            model.Name = brand.Name;
            model.CreateTime = brand.CreateTime;
            model.User = user;

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> EditBrand(int id)
        {
            var brand = _db.Brands.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            

            if (brand  is null)
                return NotFound();

            BrandViewModel model = new BrandViewModel();
            model.Id = brand.Id;
            model.Name = brand.Name;
            

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditBrand(BrandViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var brandChange = _db.Brands.FirstOrDefault(p => p.Id == model.Id && p.IsDeleted == false);
                if (brandChange  is null)
                    return NotFound();
                brandChange.Name = model.Name;
                brandChange.UserId = user.Id;
                
                _db.Entry(brandChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("Index", "Brands");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Brand brand = await _db.Brands.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (brand != null)
                {
                    return View(brand);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> DelBrand(int? id)
        {
            if (id != null)
            {
                Brand brand = await _db.Brands.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                brand.IsDeleted = true;
                _db.Entry(brand).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }

        
   
        
        
        
    }

}