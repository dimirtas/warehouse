using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class UnitMeasuresController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public UnitMeasuresController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<UnitMeasure> unitMeasures = await _db.UnitMeasures.Where(x=>x.IsDeleted == false).ToListAsync();

            List<UnitMeasureViewModel> models = new List<UnitMeasureViewModel>();

            if (unitMeasures.Count > 0)
            {
                foreach (var unitMeasure in unitMeasures)
                {
                    models.Add(new UnitMeasureViewModel()
                    {
                        Id = unitMeasure.Id,
                        Name = unitMeasure.Name
                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> AddUnitMeasure()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddUnitMeasure(UnitMeasureCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                UnitMeasure unitMeasure = new UnitMeasure()
                {
                    Name = model.Name
                };

                _db.Entry(unitMeasure).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var unitMeasure = _db.UnitMeasures.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);

            if (unitMeasure  is null)
                return NotFound();

            UnitMeasureDetailViewModel model = new UnitMeasureDetailViewModel();
            model.Id = unitMeasure.Id;
            model.Name = unitMeasure.Name;
            

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> EditUnitMeasure(int id)
        {
            var unitMeasure = _db.UnitMeasures.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);

            if (unitMeasure  is null)
                return NotFound();

            UnitMeasureViewModel model = new UnitMeasureViewModel();
            model.Id = unitMeasure.Id;
            model.Name = unitMeasure.Name;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUnitMeasure(UnitMeasureViewModel model)
        {
            if (ModelState.IsValid)
            {
                var unitMeasureChange = _db.UnitMeasures.FirstOrDefault(p => p.Id == model.Id && p.IsDeleted == false);
                if (unitMeasureChange  is null)
                    return NotFound();
                unitMeasureChange.Name = model.Name;

                _db.Entry(unitMeasureChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("Index", "UnitMeasures");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                UnitMeasure unitMeasure = await _db.UnitMeasures.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (unitMeasure != null)
                {
                    return View(unitMeasure);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> DelUnitMeasure(int? id)
        {
            if (id != null)
            {
                UnitMeasure unitMeasure = await _db.UnitMeasures.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                unitMeasure.IsDeleted = true;
                _db.Entry(unitMeasure).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }

        
   
        
        
        
    }

}