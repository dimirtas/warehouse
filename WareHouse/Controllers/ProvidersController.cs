using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class ProvidersController : Controller
    {
        private Context _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ProvidersController(Context db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Provider> providers = await _db.Providers.Where(x=>x.IsDeleted == false).ToListAsync();

            List<ProviderViewModel> models = new List<ProviderViewModel>();

            if (providers.Count > 0)
            {
                foreach (var provider in providers)
                {
                    models.Add(new ProviderViewModel()
                    {
                        Id = provider.Id,
                        Name = provider.Name,
                        Bin = provider.Bin
                    });
                }
            }

            return View(models);
        }
        
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(ProviderCreateViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
            {
                
                Provider provider = new Provider()
                {
                    Name = model.Name,
                    Bin = model.Bin,
                    UserId = user.Id
                    
                    
                };

                _db.Entry(provider).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var provider = _db.Providers.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            var user =  _db.Users.FirstOrDefault(p => p.Id == provider.UserId);

            if (provider  is null)
                return NotFound();

            ProviderDetailViewModel model = new ProviderDetailViewModel();
            model.Id = provider.Id;
            model.Bin = provider.Bin;
            model.Name = provider.Name;
            model.CreateTime = provider.CreateTime;
            model.User = user;
            

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var provider = _db.Providers.FirstOrDefault(p => p.Id == id && p.IsDeleted == false);
            

            if (provider  is null)
                return NotFound();

            ProviderViewModel model = new ProviderViewModel();
            model.Id = provider.Id;
            model.Name = provider.Name;
            model.Bin = provider.Bin;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ProviderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var providerChange = _db.Providers.FirstOrDefault(p => p.Id == model.Id && p.IsDeleted == false);
                if (providerChange  is null)
                    return NotFound();
                providerChange.Name = model.Name;
                providerChange.Bin = model.Bin;
                providerChange.UserId = user.Id;
                
                _db.Entry(providerChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("Index", "Providers");
        }
        
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Provider provider = await _db.Providers.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                if (provider != null)
                {
                    return View(provider);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                Provider provider = await _db.Providers.FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false);
                provider.IsDeleted = true;                                    // мягкое удаление
                _db.Entry(provider).State = EntityState.Modified; 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }


    }

}