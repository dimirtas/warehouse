using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WareHouse.Models;
using WareHouse.Models.Data;
using WareHouse.Models.ViewModels;

namespace WareHouse.Controllers
{
    public class ValidationController
    {
        private Context _db;
        public ValidationController(Context db)
        {
            _db = db;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckEmail(string Email)
        {
            List<User> users = _db.Users.ToList();
            var user = users.FirstOrDefault(u => u.Email == Email);
            if (user == null)
            {
                return true;
            }
            return false;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckUserName(string UserName)
        {
            List<User> users = _db.Users.ToList();
            var user = users.FirstOrDefault(u=> u.UserName == UserName);
            if (user == null)
            {
                return true;
            }
            return false;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckLogin(string Login)
        {
            List<User> users = _db.Users.ToList();
            var userEmail = users.FirstOrDefault(u=> u.Email == Login);
            var userLogin = users.FirstOrDefault(u=> u.UserName == Login);
            if (userEmail != null  || userLogin != null )
            {
                return true;
            }
            return false;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckBrand(string Name)
        {
            List<Brand> brands = _db.Brands.ToList();
            var userBrand = brands.FirstOrDefault(x=> x.Name == Name);
            
            if (userBrand != null )
            {
                return false;
            }
            return true;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckUnitMeasure(string Name)
        {
            List<UnitMeasure> list = _db.UnitMeasures.ToList();
            var userUnitMeasure = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userUnitMeasure != null )
            {
                return false;
            }
            return true;
        }
        
        public bool CheckProviderName(string Name)
        {
            List<Provider> list = _db.Providers.ToList();
            var userProvider = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userProvider != null )
            {
                return false;
            }
            return true;
        }
        
        public bool CheckProviderBin(string Bin)
        {
            
            char[] qts = Bin.ToCharArray();  // будет ли 12 символов
            if (qts.Length != 12)
            {
                return false;
            }

            double value;
            if (double.TryParse(Bin, out value))
            {
                if (value < 0)
                    return false;
                List<Provider> list = _db.Providers.ToList();
                var userProvider = list.FirstOrDefault(x=> x.Bin == Bin);
            
                if (userProvider != null )
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }
        
        public bool CheckRecipientName(string Name)
        {
            List<Recipient> list = _db.Recipients.ToList();
            var userRecipient = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userRecipient != null )
            {
                return false;
            }
            return true;
        }
        
        public bool CheckRecipientBin(string Bin)
        {
            char[] qts = Bin.ToCharArray();  // будет ли 12 символов
            if (qts.Length != 12)
            {
                return false;
            }
            
            double value;
            if (double.TryParse(Bin, out value))
            {
                if (value < 0)
                    return false;
                List<Recipient> list = _db.Recipients.ToList();
                var userRecipient = list.FirstOrDefault(x=> x.Bin == Bin);
            
                if (userRecipient != null )
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            
            return true;
        }
        
        public bool CheckProductType(string Name)
        {
            List<ProductType> list = _db.ProductTypes.ToList();
            var userProductType = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userProductType != null )
            {
                return false;
            }
            return true;
        }

        public bool CheckParameterName(string Name)
        {
            List<Parameter> list = _db.Parameters.ToList();
            var userParameter = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userParameter != null )
            {
                return false;
            }
            return true;
        }
        
        
        public bool CheckParameterNameEdit(string Name, int Id)
        {
            
            List<Parameter> list = _db.Parameters.ToList();
            var current = list.FirstOrDefault(x=> x.Id == Id);
            if (current.Name == Name )
            {
                return true;
            }
           
            var userParameter = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userParameter != null )
            {
                return false;
            }
            return true;
        }

        public bool CheckProductUniqueCode(string UniqueCode)
        {
            double value;
            if (double.TryParse(UniqueCode, out value))
            {
                if (value < 0)
                    return false;
                List<Product> list = _db.Products.ToList();
                var userProduct = list.FirstOrDefault(x=> x.UniqueCode == UniqueCode);
            
                if (userProduct != null )
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }
        
        public bool CheckProductUniqueCodeEdit(string UniqueCode, int Id)
        {
            double value;
            if (double.TryParse(UniqueCode, out value))
            {
                if (value < 0)
                    return false;
                List<Product> list = _db.Products.ToList();
                var current = list.FirstOrDefault(x=> x.Id == Id);
                if (current.UniqueCode == UniqueCode )
                    return true;

                var userProduct = list.FirstOrDefault(x=> x.UniqueCode == UniqueCode);
            
                if (userProduct != null )
                   return false;
            }
            else
            {
                return false;
            }

            return true;
        }
        
        public bool CheckProductName(string Name)
        {
            List<Product> list = _db.Products.ToList();
            var userProduct = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userProduct != null )
            {
                return false;
            }
            return true;
        }
        
        public bool CheckProductNameEdit(string Name, int Id)
        {
            
            List<Product> list = _db.Products.ToList();
            var current = list.FirstOrDefault(x=> x.Id == Id);
            if (current.Name == Name )
            {
                return true;
            }
           
            var userProduct = list.FirstOrDefault(x=> x.Name == Name);
            
            if (userProduct != null )
            {
                return false;
            }
            return true;
        }


        [AcceptVerbs("GET", "POST")]
        public bool CheckImage(string Foto)
        {
            if (Foto == null)
            {
                return true;
            }

            string ext = Foto.Substring(Foto.LastIndexOf('.'));
            if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
            {
                return true;
            }

            return false;
        }
        
        
        
        
    }
}